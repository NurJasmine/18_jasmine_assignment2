﻿using System;
using NUnit.Framework;
using BowlingGame;

namespace BowlingGameTest
{
    [TestFixture()]
    public class Test
    {
        Game game;
        [SetUp]
        public void SetUpGame()
        {
            game = new Game();
        }

        public void RollMany(int rolls, int pins)
        {
            for (int i = 0; i < 20; i++)
            {
                game.Roll(pins);
            }
        }

        [Test]
        public void RollGutter()
        {
            RollMany(20, 0);
            Assert.That(game.Score(), Is.EqualTo(0));
        }

        [Test]
        public void RollOnes()
        {
            RollMany(20, 1);
            Assert.That(game.Score(), Is.EqualTo(20));
        }

        [Test]
        public void OneSpareInFirstFrame()
        {
            game.Roll(9);
            game.Roll(1);
            RollMany(18, 1);

            Assert.That(game.Score(), Is.EqualTo(29));
        }

        [Test]
        public void OneStrikeInFirstFrame()
        {
            game.Roll(10);
            RollMany(18, 1);

            Assert.That(game.Score(), Is.EqualTo(30));
        }

        [Test]
        public void PerfectGame()
        {
            RollMany(12, 10);

            Assert.That(game.Score(), Is.EqualTo(300));
        }
    }
}
